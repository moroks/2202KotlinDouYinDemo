package com.huluobo.a2202kotlindouyindemo

import android.app.Application
import android.util.Log
import com.huluobo.base.db.AppDBUtils
import com.hyphenate.chat.EMClient
import com.hyphenate.chat.EMOptions
import com.hyphenate.easeui.EaseIM
import com.tencent.live2.V2TXLivePremier
import com.tencent.live2.V2TXLivePremier.V2TXLivePremierObserver
import com.tencent.rtmp.TXLiveBase
import com.umeng.commonsdk.UMConfigure
import com.umeng.socialize.PlatformConfig


/**
 *  Created by LC on 2024/5/27.
 */
class DouYinApp : Application() {
    override fun onCreate() {
        super.onCreate()
        AppDBUtils.init(this)

        //友盟初始化
        UMConfigure.init(
            this, "6656e54e940d5a4c4961018c", "umeng", UMConfigure.DEVICE_TYPE_PHONE, ""
        )
        PlatformConfig.setQQZone("101830139", "5d63ae8858f1caab67715ccd6c18d7a5");

        val options = EMOptions()
        options.appKey = "moroks#2202douyinkotlindemo"
        //EaseIM 初始化
        if (EaseIM.getInstance().init(this, options)) {
            //在做打包混淆时，关闭 debug 模式，避免消耗不必要的资源
            EMClient.getInstance().setDebugMode(true);
            //EaseIM 初始化成功之后再调用注册消息监听的代码 ...
        }

        val licenceURL = "https://license.vod2.myqcloud.com/license/v2/1301155780_1/v_cube.license" // 获取到的 licence url

        val licenceKey = "96a0c2ef2a0011666abd276f1ff4acaa" // 获取到的 licence key

        V2TXLivePremier.setLicence(this, licenceURL, licenceKey)
        V2TXLivePremier.setObserver(object : V2TXLivePremierObserver() {
            override fun onLicenceLoaded(result: Int, reason: String) {
                Log.i("TagA", "onLicenceLoaded: result:$result, reason:$reason")
            }
        })

    }
}