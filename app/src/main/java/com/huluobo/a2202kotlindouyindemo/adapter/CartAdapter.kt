package com.huluobo.a2202kotlindouyindemo.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.huluobo.a2202kotlindouyindemo.R
import com.huluobo.a2202kotlindouyindemo.utils.ImageUtils
import com.huluobo.base.bean.CartBean

/**
 *  Created by LC on 2024/6/6.
 */
class CartAdapter(list: MutableList<CartBean>) : BaseQuickAdapter<CartBean, BaseViewHolder>(R.layout.item_cart, list) {
    override fun convert(holder: BaseViewHolder, item: CartBean) {
        ImageUtils.loadImage(context, item.goods_default_icon, holder.getView(R.id.item_cart_icon_iv))
        holder.setText(R.id.item_cart_desc_tv, item.goods_desc)
        holder.setText(R.id.item_cart_price_tv, "${item.goods_default_price}元")
        holder.setText(R.id.item_cart_count_tv, "x ${item.count}")
    }
}