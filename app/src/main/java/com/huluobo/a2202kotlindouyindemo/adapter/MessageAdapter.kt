package com.huluobo.a2202kotlindouyindemo.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.huluobo.a2202kotlindouyindemo.R
import com.huluobo.base.bean.MessageEntity

/**
 *  Created by LC on 2024/6/4.
 */
class MessageAdapter(list: MutableList<MessageEntity>) :
    BaseQuickAdapter<MessageEntity, BaseViewHolder>(R.layout.item_chat_room_msg_layout, list) {
    override fun convert(holder: BaseViewHolder, item: MessageEntity) {
        val msg = "${item.username}:${item.content}"
        holder.setText(R.id.item_chat_room_content_tv, msg)
    }
}