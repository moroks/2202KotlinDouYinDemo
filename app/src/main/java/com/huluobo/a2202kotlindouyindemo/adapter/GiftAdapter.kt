package com.huluobo.a2202kotlindouyindemo.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.huluobo.a2202kotlindouyindemo.R
import com.huluobo.a2202kotlindouyindemo.utils.ImageUtils
import com.huluobo.base.bean.GiftBean

/**
 *  Created by LC on 2024/6/3.
 */
class GiftAdapter(list: MutableList<GiftBean>) : BaseQuickAdapter<GiftBean, BaseViewHolder>(R.layout.item_gift, list) {
    override fun convert(holder: BaseViewHolder, item: GiftBean) {
        ImageUtils.loadImage(context, item.giftpath, holder.getView(R.id.item_gift_icon_iv))
        holder.setText(R.id.item_gift_price_tv, "${item.price}抖币")
    }
}