package com.huluobo.a2202kotlindouyindemo.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.huluobo.a2202kotlindouyindemo.R
import com.huluobo.a2202kotlindouyindemo.utils.ImageUtils
import com.huluobo.base.bean.GoodsBean

/**
 *  Created by LC on 2024/6/5.
 */
class ShopAdapter(list: MutableList<GoodsBean>) :
    BaseQuickAdapter<GoodsBean, BaseViewHolder>(R.layout.item_shop_goods, list) {
    override fun convert(holder: BaseViewHolder, item: GoodsBean) {
        ImageUtils.loadImage(context, item.goods_default_icon, holder.getView(R.id.item_shop_goods_icon))
        holder.setText(R.id.item_shop_goods_desc, item.goods_desc)
        holder.setText(R.id.item_shop_goods_price, "价格:${item.goods_default_price}元")
    }
}