package com.huluobo.a2202kotlindouyindemo.adapter

import android.animation.ObjectAnimator
import android.view.View
import android.view.animation.LinearInterpolator
import android.widget.ImageView
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.huluobo.a2202kotlindouyindemo.R
import com.huluobo.a2202kotlindouyindemo.utils.ImageUtils
import com.huluobo.base.bean.VideoBean
import com.shuyu.gsyvideoplayer.video.StandardGSYVideoPlayer

/**
 *  Created by LC on 2024/5/30.
 */
class MultiVideoAdapter : BaseMultiItemQuickAdapter<VideoBean, BaseViewHolder>() {
    init {
        addItemType(0, R.layout.item_videos)
        addItemType(1, R.layout.item_videos_live)
        addItemType(2, R.layout.item_videos_full)
    }

    override fun convert(holder: BaseViewHolder, item: VideoBean) {
        val player = holder.getView<StandardGSYVideoPlayer>(R.id.item_video_gsyPlayer)
        player.backButton.visibility = View.GONE
        player.setUp(item.videopath, true, "")

        when (item.type) {
            0, 2 -> setVideoOption(item, holder)
            1 -> setLiveOption(item, holder)
        }
    }

    private fun setLiveOption(item: VideoBean, holder: BaseViewHolder) {
        setCommentVideoInfo(holder, item)
    }

    private fun setVideoOption(item: VideoBean, holder: BaseViewHolder) {
        setCommentVideoInfo(holder, item)
        //右下方区域赋值
        //头像
        ImageUtils.loadCircleImage(context, item.headpath, holder.getView(R.id.item_video_header_iv))
        //关注状态
        if (item.guanzhu == 0) {
            holder.getView<ImageView>(R.id.item_video_add_iv).visibility = View.VISIBLE
        } else {
            holder.getView<ImageView>(R.id.item_video_add_iv).visibility = View.GONE
        }
        //点赞状态
        if (item.dianzan == 0) {
            holder.setImageResource(R.id.item_video_like_iv, R.drawable.ic_xihuan)
        } else {
            holder.setImageResource(R.id.item_video_like_iv, R.drawable.ic_xihuanred)
        }
        //点赞数量   baseViewHolder.setText(R.id.item_video_like_tv, likeCount < 10000 ? String.valueOf(likeCount) :
        //                likeCount / 10000 + "W");
        holder.setText(
            R.id.item_video_like_count_tv,
            if (item.like_count < 10000) item.like_count.toString() else "${item.like_count / 10000}W"
        )
        //评论数量
        holder.setText(R.id.item_video_comment_count_tv, "3.5W")
        //收藏数量
        holder.setText(R.id.item_video_follow_count_tv, "4.5W")
        //分享数量
        holder.setText(R.id.item_video_share_count_tv, "3.2W")
        //碟片动画
        val musicPlayer = holder.getView<ImageView>(R.id.item_video_music_player_iv)
//        Glide.with(context).load(item.headpath).into(musicPlayer)
        ImageUtils.loadCircleImage(context, item.headpath, musicPlayer)

        val animator: ObjectAnimator = ObjectAnimator.ofFloat(musicPlayer, "Rotation", 0f, 360f)
        animator.duration = 3000;
        animator.repeatCount = -1
        animator.interpolator = LinearInterpolator()
        animator.start()
    }

    private fun setCommentVideoInfo(holder: BaseViewHolder, item: VideoBean) {

        //左下区域赋值
        //地址
        holder.setText(R.id.item_video_address_tv, item.address)
        //作者
        holder.setText(R.id.item_video_authname_tv, item.authname)
        //描述
        holder.setText(R.id.item_video_caption_tv, item.caption)
        //原创(跑马灯)
        holder.setText(R.id.item_video_pao_tv, "${item.authname}的原创作品")
    }
}