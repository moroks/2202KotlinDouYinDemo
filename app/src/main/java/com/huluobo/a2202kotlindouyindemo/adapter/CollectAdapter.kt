package com.huluobo.a2202kotlindouyindemo.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.huluobo.a2202kotlindouyindemo.R
import com.huluobo.a2202kotlindouyindemo.utils.ImageUtils
import com.huluobo.base.bean.VideoBean

/**
 *  Created by LC on 2024/6/11.
 */
class CollectAdapter(list: MutableList<VideoBean>) :
    BaseQuickAdapter<VideoBean, BaseViewHolder>(R.layout.item_collect, list) {
    override fun convert(holder: BaseViewHolder, item: VideoBean) {
        ImageUtils.loadImage(context, item.videomainimg, holder.getView(R.id.item_collect_video_iv))
        holder.setText(R.id.item_collect_desc_tv, item.caption)
    }
}