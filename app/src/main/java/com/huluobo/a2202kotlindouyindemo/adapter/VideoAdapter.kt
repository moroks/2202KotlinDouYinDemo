package com.huluobo.a2202kotlindouyindemo.adapter

import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.view.View
import android.view.animation.LinearInterpolator
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.huluobo.a2202kotlindouyindemo.R
import com.huluobo.a2202kotlindouyindemo.utils.ImageUtils
import com.huluobo.base.bean.VideoBean
import com.shuyu.gsyvideoplayer.video.StandardGSYVideoPlayer

/**
 *  Created by LC on 2024/5/24.
 *  构造方法:
 *  在kotlin中,一个类后面加上括号就代表是它的构造方法
 *  VideoAdapter(list: MutableList<VideoBean>)  就是当前adapter的构造方法
 *  BaseQuickAdapter<VideoBean, BaseViewHolder>(R.layout.item_videos, list) 就是被继承的万能适配器的父类的构造方法
 */
class VideoAdapter(list: MutableList<VideoBean>) :
    BaseQuickAdapter<VideoBean, BaseViewHolder>(R.layout.item_videos, list) {
    override fun convert(holder: BaseViewHolder, item: VideoBean) {
        val player = holder.getView<StandardGSYVideoPlayer>(R.id.item_video_gsyPlayer)

        player.backButton.visibility = View.GONE

        //播放地址
        player.setUp(item.videopath, true, "")

        //左下区域赋值
        //地址
        holder.setText(R.id.item_video_address_tv, item.address)
        //作者
        holder.setText(R.id.item_video_authname_tv, item.authname)
        //描述
        holder.setText(R.id.item_video_caption_tv, item.caption)
        //原创(跑马灯)
        holder.setText(R.id.item_video_pao_tv, "${item.authname}的原创作品")

        //右下方区域赋值
        //头像
//        Glide.with(context).load(item.headpath).into(holder.getView(R.id.item_video_header_iv))
        ImageUtils.loadCircleImage(context, item.headpath, holder.getView(R.id.item_video_header_iv))
        //关注状态
        if (item.guanzhu == 0) {
            holder.getView<ImageView>(R.id.item_video_add_iv).visibility = View.VISIBLE
        } else {
            holder.getView<ImageView>(R.id.item_video_add_iv).visibility = View.GONE
        }
        //点赞状态
        if (item.dianzan == 0) {
            holder.setImageResource(R.id.item_video_like_iv, R.drawable.ic_xihuan)
        } else {
            holder.setImageResource(R.id.item_video_like_iv, R.drawable.ic_xihuanred)
        }
        //点赞数量   baseViewHolder.setText(R.id.item_video_like_tv, likeCount < 10000 ? String.valueOf(likeCount) :
        //                likeCount / 10000 + "W");
        holder.setText(
            R.id.item_video_like_count_tv,
            if (item.like_count < 10000) item.like_count.toString() else "${item.like_count / 10000}W"
        )
        //评论数量
        holder.setText(R.id.item_video_comment_count_tv, "3.5W")
        //收藏数量
        holder.setText(R.id.item_video_follow_count_tv, "4.5W")
        //分享数量
        holder.setText(R.id.item_video_share_count_tv, "3.2W")
        //碟片动画
        val musicPlayer = holder.getView<ImageView>(R.id.item_video_music_player_iv)
//        Glide.with(context).load(item.headpath).into(musicPlayer)
        ImageUtils.loadCircleImage(context, item.headpath, musicPlayer)

        val animator: ObjectAnimator = ObjectAnimator.ofFloat(musicPlayer, "Rotation", 0f, 360f)
        animator.duration = 3000;
        animator.repeatCount = -1
        animator.interpolator = LinearInterpolator()
        animator.start()
    }
}