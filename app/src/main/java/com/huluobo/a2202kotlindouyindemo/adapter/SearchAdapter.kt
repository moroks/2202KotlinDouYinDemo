package com.huluobo.a2202kotlindouyindemo.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.huluobo.a2202kotlindouyindemo.R
import com.huluobo.base.bean.SearchBeanResult

/**
 *  Created by LC on 2024/6/7.
 */
class SearchAdapter(list: MutableList<SearchBeanResult>) :
    BaseQuickAdapter<SearchBeanResult, BaseViewHolder>(R.layout.item_search, list) {
    override fun convert(holder: BaseViewHolder, item: SearchBeanResult) {
        holder.setText(R.id.item_search_tv, item.keyword)
    }
}