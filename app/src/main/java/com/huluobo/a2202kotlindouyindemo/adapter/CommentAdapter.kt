package com.huluobo.a2202kotlindouyindemo.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.huluobo.a2202kotlindouyindemo.R
import com.huluobo.a2202kotlindouyindemo.utils.ImageUtils
import com.huluobo.base.bean.CommentBean

/**
 *  Created by LC on 2024/5/28.
 */
class CommentAdapter(val list: MutableList<CommentBean>) :
    BaseQuickAdapter<CommentBean, BaseViewHolder>(R.layout.item_comment, list) {
    override fun convert(holder: BaseViewHolder, item: CommentBean) {
        ImageUtils.loadImage(context, item.icon, holder.getView(R.id.item_comment_icon))
        holder.setText(R.id.item_comment_name, item.username)
        holder.setText(R.id.item_comment_msg, item.msg)
        holder.setText(R.id.item_comment_time, item.createtime)
    }
}