package com.huluobo.a2202kotlindouyindemo.cart

import androidx.lifecycle.MutableLiveData
import com.huluobo.base.bean.CartBean
import com.huluobo.base.bean.CartBeanResult
import com.huluobo.base.model.BaseRepo
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 *  Created by LC on 2024/6/5.
 */
class CartRepo : BaseRepo() {
    fun getCartList(success: MutableLiveData<CartBeanResult>, failed: MutableLiveData<String>) {
        apiService.getCartList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<CartBeanResult> {
                override fun onSubscribe(d: Disposable) {
                }

                override fun onError(e: Throwable) {
                    failed.value = e.message
                }

                override fun onComplete() {
                }

                override fun onNext(t: CartBeanResult) {
                    success.value = t
                }
            })
    }
}