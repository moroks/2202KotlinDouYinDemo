package com.huluobo.a2202kotlindouyindemo.cart

import androidx.lifecycle.MutableLiveData
import com.huluobo.base.bean.CartBeanResult
import com.huluobo.base.viewModel.BaseViewModel

/**
 *  Created by LC on 2024/6/5.
 */
class CartViewModel : BaseViewModel<CartRepo>() {
    val getCartListSuccess = MutableLiveData<CartBeanResult>()
    val getCartListFailed = MutableLiveData<String>()

    fun getCartList() {
        repo.getCartList(getCartListSuccess, getCartListFailed)
    }

    override fun createRepo(): CartRepo = CartRepo()
}