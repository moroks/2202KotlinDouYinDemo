package com.huluobo.a2202kotlindouyindemo.pay

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.huluobo.a2202kotlindouyindemo.R
import com.huluobo.a2202kotlindouyindemo.databinding.ActivityCancelPayBinding
import com.huluobo.base.view.BaseMVVMActivity

class CancelPayActivity : BaseMVVMActivity<ActivityCancelPayBinding>() {

    override fun initData() {
    }

    override fun initView() {
        dataBinding.cancelBackBtn.setOnClickListener {
            finish()
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_cancel_pay
}