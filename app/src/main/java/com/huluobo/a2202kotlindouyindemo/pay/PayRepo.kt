package com.huluobo.a2202kotlindouyindemo.pay

import androidx.lifecycle.MutableLiveData
import com.huluobo.base.bean.Money
import com.huluobo.base.model.BaseRepo
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 *  Created by LC on 2024/6/4.
 */
class PayRepo : BaseRepo() {
    fun updateUserMoney(money: Money) {
        db.getMoneyDao().updateUserMoney(money)
    }

    fun queryUserMoney(username: String, success: MutableLiveData<Money>, failed: MutableLiveData<String>) {
        db.getMoneyDao().queryUserMoneyByName(username)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<Money> {
                override fun onSubscribe(d: Disposable) {
                }

                override fun onError(e: Throwable) {
                    failed.value = e.message
                }

                override fun onComplete() {
                }

                override fun onNext(t: Money) {
                    success.value = t
                }
            })
    }
}