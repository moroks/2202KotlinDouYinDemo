package com.huluobo.a2202kotlindouyindemo.pay

import androidx.lifecycle.MutableLiveData
import com.huluobo.base.bean.Money
import com.huluobo.base.viewModel.BaseViewModel

/**
 *  Created by LC on 2024/6/4.
 */
class PayViewModel : BaseViewModel<PayRepo>() {

    fun updateUserMoney(money: Money) {
        repo.updateUserMoney(money)
    }


    val queryUserMoneySuccess = MutableLiveData<Money>()
    val queryUserMoneyFailed = MutableLiveData<String>()

    fun queryUserMoney(username: String) {
        repo.queryUserMoney(username, queryUserMoneySuccess, queryUserMoneyFailed)
    }

    override fun createRepo(): PayRepo = PayRepo()
}