package com.huluobo.a2202kotlindouyindemo.utils

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.huluobo.a2202kotlindouyindemo.R

/**
 *  Created by LC on 2024/5/27.
 */
object ImageUtils {
    fun loadImage(context: Context, url: String, imageView: ImageView) {
        Glide.with(context)
            .load(url)
            .placeholder(ColorDrawable(Color.GRAY))
            .error(R.drawable.baseline_error_24)
            .skipMemoryCache(true)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .transform(CenterCrop())
            .into(imageView)
    }

    fun loadCircleImage(context: Context, url: String, imageView: ImageView) {
        Glide.with(context)
            .load(url)
            .placeholder(ColorDrawable(Color.GRAY))
            .error(R.drawable.baseline_error_24)
            .skipMemoryCache(true)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .transform(CenterCrop(), CircleCrop())
            .into(imageView)
    }

    fun loadRoundImage(context: Context, url: String, imageView: ImageView, radius: Int) {
        Glide.with(context)
            .load(url)
            .placeholder(ColorDrawable(Color.GRAY))
            .error(R.drawable.baseline_error_24)
            .skipMemoryCache(true)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .transform(CenterCrop(), RoundedCorners(radius))
            .into(imageView)
    }

}