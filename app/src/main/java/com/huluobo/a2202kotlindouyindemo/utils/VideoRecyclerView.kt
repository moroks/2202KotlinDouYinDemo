package com.huluobo.a2202kotlindouyindemo.utils

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.recyclerview.widget.RecyclerView

/**
 *  Created by LC on 2024/5/28.
 */
class VideoRecyclerView(context: Context, attributeSet: AttributeSet) : RecyclerView(context, attributeSet) {
    private var isIntercept = true
    fun setIntercept(interceptor: Boolean) {
        isIntercept = interceptor
    }

    override fun onInterceptTouchEvent(e: MotionEvent?): Boolean {
        if (!isIntercept) {
            return false
        }
        return super.onInterceptTouchEvent(e)
    }
}