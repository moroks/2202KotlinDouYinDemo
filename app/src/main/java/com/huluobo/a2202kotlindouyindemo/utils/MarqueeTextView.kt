package com.huluobo.a2202kotlindouyindemo.utils

import android.content.Context
import android.text.TextUtils
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView

/**
 *  Created by LC on 2024/5/24.
 *  自定义跑马灯效果的textView
 */
class MarqueeTextView(context: Context, attr: AttributeSet) : AppCompatTextView(context, attr) {
    init {
        ellipsize = TextUtils.TruncateAt.MARQUEE
        isSingleLine = true
        marqueeRepeatLimit = -1
    }

    override fun isFocused(): Boolean = true
}