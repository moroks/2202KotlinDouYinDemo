package com.huluobo.a2202kotlindouyindemo.startLive

import android.util.Log
import com.huluobo.a2202kotlindouyindemo.R
import com.huluobo.a2202kotlindouyindemo.databinding.ActivityStartLiveBinding
import com.huluobo.base.view.BaseMVVMActivity
import com.tencent.live2.V2TXLiveCode.V2TXLIVE_ERROR_INVALID_LICENSE
import com.tencent.live2.V2TXLiveDef
import com.tencent.live2.V2TXLivePusher
import com.tencent.live2.impl.V2TXLivePusherImpl


class StartLiveActivity : BaseMVVMActivity<ActivityStartLiveBinding>() {
    override fun initData() {
    }

    override fun initView() {
        // 指定对应的直播协议为 RTMP，该协议不支持连麦
        val mLivePusher: V2TXLivePusher = V2TXLivePusherImpl(this, V2TXLiveDef.V2TXLiveMode.TXLiveMode_RTMP)
        mLivePusher.setRenderView(dataBinding.pusherTxCloudView)
        mLivePusher.startCamera(true)
        mLivePusher.startMicrophone()

        dataBinding.startLiveStartPushBtn.setOnClickListener {
            // 根据推流协议传入对应的 URL 即可启动推流， RTMP 协议以 rtmp:// 开头，该协议不支持连麦
            val url =
                "rtmp://200565.push.tlivecloud.com/live/123?txSecret=add133a81db277b67118cdd11a86b5de&txTime=66612491"
            val ret = mLivePusher.startPush(url)
            if (ret == V2TXLIVE_ERROR_INVALID_LICENSE) {
                Log.i("TagA", "startRTMPPush: license 校验失败")
            }
        }

        dataBinding.startLiveStopPushBtn.setOnClickListener {
            // 结束推流
            mLivePusher.stopPush();
        }
    }

    //

    override fun getLayoutId(): Int = R.layout.activity_start_live
}