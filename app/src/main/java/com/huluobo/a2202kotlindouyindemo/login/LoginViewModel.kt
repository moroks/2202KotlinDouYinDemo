package com.huluobo.a2202kotlindouyindemo.login

import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.huluobo.base.bean.UserBeanResult
import com.huluobo.base.net.Const
import com.huluobo.base.viewModel.BaseViewModel
import okhttp3.MediaType
import okhttp3.RequestBody

/**
 *  Created by LC on 2024/5/23.
 */
class LoginViewModel : BaseViewModel<LoginRepo>() {
    //    private val repo = LoginRepo()
    override fun createRepo(): LoginRepo = LoginRepo()

    val loginSuccess = MutableLiveData<UserBeanResult>()

    //    MutableLiveData<UserBeanResult> success =new MutableLiveData<UserBeanResult>()
    val loginFailed = MutableLiveData<String>()

    fun login(username: String, password: String) {
        val map = mutableMapOf<String, String>()
        map[Const.PARAM_USERNAME] = username
        map[Const.PARAM_PASSWORD] = password
        val json = Gson().toJson(map)
        val body = RequestBody.create(MediaType.parse(Const.MEDIA_TYPE), json)
        repo.login(body, loginSuccess, loginFailed)
    }
}