package com.huluobo.a2202kotlindouyindemo.login

import androidx.lifecycle.MutableLiveData
import com.huluobo.base.bean.UserBeanResult
import com.huluobo.base.model.BaseRepo
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.RequestBody

/**
 *  Created by LC on 2024/5/23.
 */
class LoginRepo : BaseRepo() {
    fun login(body: RequestBody, success: MutableLiveData<UserBeanResult>, failed: MutableLiveData<String>) {
        apiService.login(body)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<UserBeanResult> {
                override fun onSubscribe(d: Disposable) {
                }

                override fun onError(e: Throwable) {
                    failed.value = e.message
                }

                override fun onComplete() {
                }

                override fun onNext(t: UserBeanResult) {
                    success.value = t
                }
            })
    }
}