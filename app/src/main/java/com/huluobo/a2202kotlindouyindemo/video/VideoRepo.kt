package com.huluobo.a2202kotlindouyindemo.video

import androidx.lifecycle.MutableLiveData
import com.huluobo.base.bean.CommentBeanResult
import com.huluobo.base.bean.FollowBeanResult
import com.huluobo.base.bean.LikeBeanResult
import com.huluobo.base.bean.VideoBean
import com.huluobo.base.bean.VideoBeanResult
import com.huluobo.base.model.BaseRepo
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.RequestBody

/**
 *  Created by LC on 2024/5/24.
 */
class VideoRepo : BaseRepo() {
    fun getVideos(
        currentPage: Int,
        pageSize: Int,
        success: MutableLiveData<VideoBeanResult>,
        failed: MutableLiveData<String>
    ) {
        apiService.getVideos(currentPage, pageSize)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<VideoBeanResult> {
                override fun onSubscribe(d: Disposable) {
                }

                override fun onError(e: Throwable) {
                    failed.value = e.message
                }

                override fun onComplete() {
                }

                override fun onNext(t: VideoBeanResult) {
                    success.value = t
                }
            })
    }

    fun likeVideo(videoId: Int, success: MutableLiveData<LikeBeanResult>, faield: MutableLiveData<String>) {
        apiService.likeVideo(videoId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<LikeBeanResult> {
                override fun onSubscribe(d: Disposable) {
                }

                override fun onError(e: Throwable) {
                    faield.value = e.message
                }

                override fun onComplete() {
                }

                override fun onNext(t: LikeBeanResult) {
                    success.value = t
                }
            })
    }

    fun noLikeVideo(videoId: Int, success: MutableLiveData<LikeBeanResult>, failed: MutableLiveData<String>) {
        apiService.noLikeVideo(videoId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<LikeBeanResult> {
                override fun onSubscribe(d: Disposable) {
                }

                override fun onError(e: Throwable) {
                    failed.value = e.message
                }

                override fun onComplete() {
                }

                override fun onNext(t: LikeBeanResult) {
                    success.value = t
                }
            })
    }

    fun followAuth(body: RequestBody, success: MutableLiveData<FollowBeanResult>, failed: MutableLiveData<String>) {
        apiService.followAuth(body)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<FollowBeanResult> {
                override fun onSubscribe(d: Disposable) {
                }

                override fun onError(e: Throwable) {
                    failed.value = e.message
                }

                override fun onComplete() {
                }

                override fun onNext(t: FollowBeanResult) {
                    success.value = t
                }
            })
    }

    fun getComments(videoId: Int, success: MutableLiveData<CommentBeanResult>, failed: MutableLiveData<String>) {
        apiService.getComment(videoId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<CommentBeanResult> {
                override fun onSubscribe(d: Disposable) {
                }

                override fun onError(e: Throwable) {
                    failed.value = e.message
                }

                override fun onComplete() {
                }

                override fun onNext(t: CommentBeanResult) {
                    success.value = t
                }
            })
    }

    fun sendComment(body: RequestBody, success: MutableLiveData<CommentBeanResult>, failed: MutableLiveData<String>) {
        apiService.sendComment(body)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<CommentBeanResult> {
                override fun onSubscribe(d: Disposable) {
                }

                override fun onError(e: Throwable) {
                    failed.value = e.message
                }

                override fun onComplete() {
                }

                override fun onNext(t: CommentBeanResult) {
                    success.value = t
                }
            })
    }

    fun insertVideo(videoBean: VideoBean, success: MutableLiveData<String>, failed: MutableLiveData<String>) {
        try {
            db.getVideoDao().insertVideo(videoBean)
            success.value = "收藏成功"
        } catch (e: Exception) {
            failed.value = e.message
        }
    }
}