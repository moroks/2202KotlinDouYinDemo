package com.huluobo.a2202kotlindouyindemo.video

import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.huluobo.base.bean.CommentBeanResult
import com.huluobo.base.bean.FollowBeanResult
import com.huluobo.base.bean.LikeBeanResult
import com.huluobo.base.bean.VideoBean
import com.huluobo.base.bean.VideoBeanResult
import com.huluobo.base.net.Const
import com.huluobo.base.viewModel.BaseViewModel
import okhttp3.MediaType
import okhttp3.RequestBody

/**
 *  Created by LC on 2024/5/24.
 */
class VideoViewModel : BaseViewModel<VideoRepo>() {

    val getVideoListSuccess = MutableLiveData<VideoBeanResult>()

    val getVideoListFailed = MutableLiveData<String>()

    val likeVideoSuccess = MutableLiveData<LikeBeanResult>()
    val noLikeVideoSuccess = MutableLiveData<LikeBeanResult>()

    val likeVideoFailed = MutableLiveData<String>()
    val noLikeVideoFailed = MutableLiveData<String>()

    val followAuthSuccess = MutableLiveData<FollowBeanResult>()
    val followAuthFailed = MutableLiveData<String>()

    val getCommentSuccess = MutableLiveData<CommentBeanResult>()
    val getCommentFailed = MutableLiveData<String>()

    val sendCommentSuccess = MutableLiveData<CommentBeanResult>()
    val sendCommentFailed = MutableLiveData<String>()

    val followSuccess = MutableLiveData<String>()
    val followFailed = MutableLiveData<String>()

    fun followVideo(videoBean: VideoBean) {
        repo.insertVideo(videoBean, followSuccess, followFailed)
    }

    fun sendComment(msg: String, videoId: Int) {
        val map = mutableMapOf<String, Any>()
        map[Const.PARAM_MSG] = msg
        map[Const.PARAM_VIDEO_ID] = videoId
        val json = Gson().toJson(map)
        val body = RequestBody.create(MediaType.parse(Const.MEDIA_TYPE), json)
        repo.sendComment(body, sendCommentSuccess, sendCommentFailed)
    }

    fun getComments(videoId: Int) {
        repo.getComments(videoId, getCommentSuccess, getCommentFailed)
    }

    fun followAuth(authname: String) {
        val map = mutableMapOf<String, String>()
        map[Const.PARAM_AUTHNAME] = authname
        val json = Gson().toJson(map)
        val body = RequestBody.create(MediaType.parse(Const.MEDIA_TYPE), json)
        repo.followAuth(body, followAuthSuccess, followAuthFailed)
    }

    fun likeVideo(videoId: Int) {
        repo.likeVideo(videoId, likeVideoSuccess, likeVideoFailed)
    }

    fun noLikeVideo(videoId: Int) {
        repo.noLikeVideo(videoId, noLikeVideoSuccess, noLikeVideoFailed)
    }


    fun getVideoList(currentPage: Int, pageSize: Int) {
        repo.getVideos(currentPage, pageSize, getVideoListSuccess, getVideoListFailed)
    }

    override fun createRepo(): VideoRepo = VideoRepo()
}