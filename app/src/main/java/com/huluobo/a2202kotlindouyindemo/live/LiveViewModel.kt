package com.huluobo.a2202kotlindouyindemo.live

import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.huluobo.base.bean.FollowBeanResult
import com.huluobo.base.bean.GiftBeanResult
import com.huluobo.base.bean.Money
import com.huluobo.base.net.Const
import com.huluobo.base.viewModel.BaseViewModel
import okhttp3.MediaType
import okhttp3.RequestBody

/**
 *  Created by LC on 2024/5/30.
 */
class LiveViewModel : BaseViewModel<LiveRepo>() {
    val followAuthSuccess = MutableLiveData<FollowBeanResult>()
    val followAuthFailed = MutableLiveData<String>()

    val noFollowAuthSuccess = MutableLiveData<FollowBeanResult>()
    val noFollowAuthFailed = MutableLiveData<String>()

    val getGiftListSuccess = MutableLiveData<GiftBeanResult>()
    val getGiftListFailed = MutableLiveData<String>()

    val queryUserMoneySuccess = MutableLiveData<Money>()
    val queryUserMoneyFailed = MutableLiveData<String>()

    fun updateUserMoney(money: Money) {
        repo.updateUserMoney(money)
    }

    fun queryUserMoney(username: String) {
        repo.queryUserMoney(username, queryUserMoneySuccess, queryUserMoneyFailed)
    }

    fun getGiftList() {
        repo.getGIftList(getGiftListSuccess, getGiftListFailed)
    }

    fun followAuth(authname: String) {
        val map = mutableMapOf<String, String>()
        map[Const.PARAM_AUTHNAME] = authname
        val json = Gson().toJson(map)
        val body = RequestBody.create(MediaType.parse(Const.MEDIA_TYPE), json)
        repo.followAuth(body, followAuthSuccess, followAuthFailed)
    }


    fun noFollowAuth(authname: String) {
        val map = mutableMapOf<String, String>()
        map[Const.PARAM_AUTHNAME] = authname
        val json = Gson().toJson(map)
        val body = RequestBody.create(MediaType.parse(Const.MEDIA_TYPE), json)
        repo.noFollowAuth(body, noFollowAuthSuccess, noFollowAuthFailed)
    }


    override fun createRepo(): LiveRepo = LiveRepo()
}