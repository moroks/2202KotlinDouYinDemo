package com.huluobo.a2202kotlindouyindemo.live

import androidx.lifecycle.MutableLiveData
import com.huluobo.base.bean.FollowBeanResult
import com.huluobo.base.bean.GiftBeanResult
import com.huluobo.base.bean.Money
import com.huluobo.base.model.BaseRepo
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.RequestBody

/**
 *  Created by LC on 2024/5/30.
 */
class LiveRepo : BaseRepo() {
    fun followAuth(body: RequestBody, success: MutableLiveData<FollowBeanResult>, failed: MutableLiveData<String>) {
        apiService.followAuth(body)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<FollowBeanResult> {
                override fun onSubscribe(d: Disposable) {
                }

                override fun onError(e: Throwable) {
                    failed.value = e.message
                }

                override fun onComplete() {
                }

                override fun onNext(t: FollowBeanResult) {
                    success.value = t
                }
            })
    }

    fun noFollowAuth(body: RequestBody, success: MutableLiveData<FollowBeanResult>, failed: MutableLiveData<String>) {
        apiService.noFollowAuth(body)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<FollowBeanResult> {
                override fun onSubscribe(d: Disposable) {
                }

                override fun onError(e: Throwable) {
                    failed.value = e.message
                }

                override fun onComplete() {
                }

                override fun onNext(t: FollowBeanResult) {
                    success.value = t
                }
            })
    }

    fun getGIftList(success: MutableLiveData<GiftBeanResult>, failed: MutableLiveData<String>) {
        apiService.getGiftList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<GiftBeanResult> {
                override fun onSubscribe(d: Disposable) {
                }

                override fun onError(e: Throwable) {
                    failed.value = e.message
                }

                override fun onComplete() {
                }

                override fun onNext(t: GiftBeanResult) {
                    success.value = t
                }
            })
    }

    fun queryUserMoney(username: String, success: MutableLiveData<Money>, failed: MutableLiveData<String>) {
        db.getMoneyDao().queryUserMoneyByName(username)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<Money> {
                override fun onSubscribe(d: Disposable) {
                }

                override fun onError(e: Throwable) {
                    failed.value = e.message
                }

                override fun onComplete() {
                }

                override fun onNext(t: Money) {
                    success.value = t
                }
            })
    }

    fun updateUserMoney(money: Money) {
        db.getMoneyDao().updateUserMoney(money)
    }
}