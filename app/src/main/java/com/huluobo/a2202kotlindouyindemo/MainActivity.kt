package com.huluobo.a2202kotlindouyindemo

import android.Manifest.permission.CAMERA
import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.Manifest.permission.RECORD_AUDIO
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.content.Intent
import android.os.Handler
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.huluobo.a2202kotlindouyindemo.adapter.CommonPagerAdapter
import com.huluobo.a2202kotlindouyindemo.databinding.ActivityMainBinding
import com.huluobo.a2202kotlindouyindemo.home.HomeFragment
import com.huluobo.a2202kotlindouyindemo.login.LoginActivity
import com.huluobo.a2202kotlindouyindemo.message.MessageFragment
import com.huluobo.a2202kotlindouyindemo.mine.MineFragment
import com.huluobo.a2202kotlindouyindemo.startLive.StartLiveActivity
import com.huluobo.a2202kotlindouyindemo.video.VideoFragment
import com.huluobo.base.view.BaseMVVMActivity
import java.util.Timer
import java.util.TimerTask

class MainActivity : BaseMVVMActivity<ActivityMainBinding>() {
    private lateinit var commonPagerAdapter: CommonPagerAdapter

    private val fragments = mutableListOf<Fragment>()

    override fun initData() {
        fragments.clear()
        fragments.add(HomeFragment())
        fragments.add(VideoFragment())
        fragments.add(MessageFragment())
        fragments.add(MineFragment())

        commonPagerAdapter = CommonPagerAdapter(this, fragments)
        dataBinding.mainVp2.adapter = commonPagerAdapter
    }

    override fun initView() {
        requestPermissions(arrayOf(RECORD_AUDIO, CAMERA, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE), 102)

        //设置vp2与底部导航联动
        dataBinding.mainVp2.registerOnPageChangeCallback(object : OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                when (position) {
                    0 -> dataBinding.mainNav.menu.getItem(0).isChecked = true
                    1 -> dataBinding.mainNav.menu.getItem(1).isChecked = true
                    2 -> dataBinding.mainNav.menu.getItem(3).isChecked = true
                    3 -> dataBinding.mainNav.menu.getItem(4).isChecked = true
                }
            }
        })

        //设置底部导航和vp2联动
        dataBinding.mainNav.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.main_tab_home -> dataBinding.mainVp2.currentItem = 0
                R.id.main_tab_video -> dataBinding.mainVp2.currentItem = 1
                R.id.main_tab_start_live -> startActivity(Intent(this, StartLiveActivity::class.java))
                R.id.main_tab_message -> dataBinding.mainVp2.currentItem = 2
                R.id.main_tab_mine -> dataBinding.mainVp2.currentItem = 3
            }
            true
        }
    }

    override fun onResume() {
        super.onResume()
        dataBinding.mainVp2.currentItem = 1
        dataBinding.mainNav.menu.getItem(1).isChecked = true
    }

    override fun getLayoutId(): Int = R.layout.activity_main
}


