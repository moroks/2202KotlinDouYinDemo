package com.huluobo.a2202kotlindouyindemo.home

import androidx.lifecycle.MutableLiveData
import com.huluobo.base.bean.SearchBeanResult
import com.huluobo.base.bean.VideoBeanResult
import com.huluobo.base.viewModel.BaseViewModel

/**
 *  Created by LC on 2024/6/7.
 */
class HomeViewModel : BaseViewModel<HomeRepo>() {
    val getVideoListSuccess = MutableLiveData<VideoBeanResult>()
    val getVideoListFailed = MutableLiveData<String>()

    val findVideoSuccess = MutableLiveData<VideoBeanResult>()
    val findVideoFailed = MutableLiveData<String>()

    val querySearchSuccess = MutableLiveData<List<SearchBeanResult>>()
    val querySearchFailed = MutableLiveData<String>()

    fun querySearch() {
        repo.querySearch(querySearchSuccess, querySearchFailed)
    }

    fun saveSearch(keyword: String) {
        val searchBeanResult = SearchBeanResult(keyword, 0)
        repo.saveSearch(searchBeanResult)
    }

    fun findVideos(keyword: String) {
        repo.findVideos(keyword, findVideoSuccess, findVideoFailed)
    }

    fun getVideoList(currentPage: Int, pageSize: Int) {
        repo.getVideoList(currentPage, pageSize, getVideoListSuccess, getVideoListFailed)
    }

    override fun createRepo(): HomeRepo = HomeRepo()
}