package com.huluobo.a2202kotlindouyindemo.home

import android.text.TextUtils
import android.util.Log
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.blankj.utilcode.util.ToastUtils
import com.huluobo.a2202kotlindouyindemo.R
import com.huluobo.a2202kotlindouyindemo.adapter.HomeVideoAdapter
import com.huluobo.a2202kotlindouyindemo.adapter.SearchAdapter
import com.huluobo.a2202kotlindouyindemo.databinding.FragmentHomeBinding
import com.huluobo.base.bean.SearchBeanResult
import com.huluobo.base.bean.VideoBean
import com.huluobo.base.view.BaseMVVMFragment

class HomeFragment : BaseMVVMFragment<FragmentHomeBinding>() {
    private lateinit var viewModel: HomeViewModel
    private lateinit var homeVideoAdapter: HomeVideoAdapter
    private val videoList = mutableListOf<VideoBean>()
    private val searchList = mutableListOf<SearchBeanResult>()
    private lateinit var searchAdapter: SearchAdapter

    override fun getLayoutId(): Int = R.layout.fragment_home

    override fun initData() {
        viewModel = ViewModelProvider(requireActivity())[HomeViewModel::class.java]
        viewModel.getVideoList(1, 20)

        viewModel.getVideoListSuccess.observe(this) {
            if (it.code == 200) {
                videoList.clear()
                videoList.addAll(it.data)
                homeVideoAdapter.notifyDataSetChanged()
            } else {
                ToastUtils.showLong(it.message)
            }
        }

        viewModel.getVideoListFailed.observe(this) {
            Log.i("TagA", "首页请求视频列表失败:$it")
        }

        viewModel.findVideoSuccess.observe(this) {
            if (it.code == 200) {
                videoList.clear()
                videoList.addAll(it.data)
                homeVideoAdapter.notifyDataSetChanged()
            } else {
                ToastUtils.showLong(it.message)
            }
        }

        viewModel.findVideoFailed.observe(this) {
            Log.i("TagA", "首页查找视频列表失败:$it")
        }

        viewModel.querySearchSuccess.observe(this) {
            searchList.clear()
            searchList.addAll(it)
            searchAdapter.notifyDataSetChanged()
        }

        viewModel.querySearchFailed.observe(this) {
            Log.i("TagA", "查询搜索记录失败:$it")
        }
    }

    override fun initView() {
        dataBinding.homeRv.layoutManager = GridLayoutManager(requireContext(), 2)
        homeVideoAdapter = HomeVideoAdapter(videoList)
        dataBinding.homeRv.adapter = homeVideoAdapter

        dataBinding.homeSearchBtn.setOnClickListener {
            val keywords = dataBinding.homeSearchEt.text.toString().trim()
            if (keywords.isNotEmpty()) {
                viewModel.findVideos(keywords)
                //保存搜索记录
                viewModel.saveSearch(keywords)
            } else {
                viewModel.getVideoList(1, 20)
            }
        }

        dataBinding.homeSearchQueryBtn.setOnClickListener {
            viewModel.querySearch()
        }

        dataBinding.homeSearchRv.layoutManager = GridLayoutManager(requireContext(), 5)
        searchAdapter = SearchAdapter(searchList)
        dataBinding.homeSearchRv.adapter = searchAdapter
    }
}