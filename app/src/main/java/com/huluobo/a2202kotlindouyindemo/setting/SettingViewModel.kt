package com.huluobo.a2202kotlindouyindemo.setting

import com.huluobo.base.viewModel.BaseViewModel

/**
 *  Created by LC on 2024/6/11.
 */
class SettingViewModel : BaseViewModel<SettingRepo>() {
    override fun createRepo(): SettingRepo = SettingRepo()
}