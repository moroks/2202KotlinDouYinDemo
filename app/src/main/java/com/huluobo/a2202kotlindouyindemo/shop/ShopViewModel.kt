package com.huluobo.a2202kotlindouyindemo.shop

import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.huluobo.base.bean.AddToCartBeanResult
import com.huluobo.base.bean.GoodsBeanResult
import com.huluobo.base.net.Const
import com.huluobo.base.viewModel.BaseViewModel
import okhttp3.MediaType
import okhttp3.RequestBody

/**
 *  Created by LC on 2024/6/5.
 */
class ShopViewModel : BaseViewModel<ShopRepo>() {

    val getGoodsListSuccess = MutableLiveData<GoodsBeanResult>()
    val getGoodsListFailed = MutableLiveData<String>()

    val addToCartSuccess = MutableLiveData<AddToCartBeanResult>()
    val addToCartFailed = MutableLiveData<String>()

    fun addToCart(goodsId: Int, count: Int) {
        val map = mutableMapOf<String, Int>()
        map[Const.PARAM_GOODS_ID] = goodsId
        map[Const.PARAM_COUNT] = count

        val json = Gson().toJson(map)
        val body = RequestBody.create(MediaType.parse(Const.MEDIA_TYPE), json)
        repo.addToCart(body, addToCartSuccess, addToCartFailed)
    }

    fun getGoodsList(
        categoryId: Int,
        currentPage: Int,
        pageSize: Int
    ) {
        repo.getGoodsList(categoryId, currentPage, pageSize, getGoodsListSuccess, getGoodsListFailed)
    }

    override fun createRepo(): ShopRepo = ShopRepo()
}