package com.huluobo.a2202kotlindouyindemo.shop

import androidx.lifecycle.MutableLiveData
import com.huluobo.base.bean.AddToCartBeanResult
import com.huluobo.base.bean.GoodsBeanResult
import com.huluobo.base.model.BaseRepo
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.RequestBody

/**
 *  Created by LC on 2024/6/5.
 */
class ShopRepo : BaseRepo() {
    fun getGoodsList(
        categoryId: Int,
        currentPage: Int,
        pageSize: Int,
        success: MutableLiveData<GoodsBeanResult>,
        failed: MutableLiveData<String>
    ) {
        apiService.getGoodsList(categoryId, currentPage, pageSize)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<GoodsBeanResult> {
                override fun onSubscribe(d: Disposable) {
                }

                override fun onError(e: Throwable) {
                    failed.value = e.message
                }

                override fun onComplete() {
                }

                override fun onNext(t: GoodsBeanResult) {
                    success.value = t
                }
            })
    }

    fun addToCart(body: RequestBody, success: MutableLiveData<AddToCartBeanResult>, failed: MutableLiveData<String>) {
        apiService.addToCart(body)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<AddToCartBeanResult> {
                override fun onSubscribe(d: Disposable) {
                }

                override fun onError(e: Throwable) {
                    failed.value = e.message
                }

                override fun onComplete() {
                }

                override fun onNext(t: AddToCartBeanResult) {
                    success.value = t
                }

            })
    }
}