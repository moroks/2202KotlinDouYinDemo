package com.huluobo.a2202kotlindouyindemo.mine

import androidx.lifecycle.MutableLiveData
import com.huluobo.base.bean.LikeBeanResult
import com.huluobo.base.bean.UserBean
import com.huluobo.base.bean.VideoBean
import com.huluobo.base.bean.VideoBeanResult
import com.huluobo.base.model.BaseRepo
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 *  Created by LC on 2024/6/11.
 */
class MineRepo : BaseRepo() {
    fun getUserInfo(username: String, success: MutableLiveData<UserBean>, failed: MutableLiveData<String>) {
        db.getUserInfoDao().queryUserInfoByName(username)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<UserBean> {
                override fun onSubscribe(d: Disposable) {
                }

                override fun onError(e: Throwable) {
                    failed.value = e.message
                }

                override fun onComplete() {
                }

                override fun onNext(t: UserBean) {
                    success.value = t
                }
            })
    }

    fun getCollectVideos(success: MutableLiveData<List<VideoBean>>, failed: MutableLiveData<String>) {
        db.getVideoDao().queryVideos()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<List<VideoBean>> {
                override fun onSubscribe(d: Disposable) {
                }

                override fun onError(e: Throwable) {
                    failed.value = e.message
                }

                override fun onComplete() {
                }

                override fun onNext(t: List<VideoBean>) {
                    success.value = t
                }
            })
    }

    fun deleteCollectVideo(videoBean: VideoBean) {
        db.getVideoDao().deleteVideo(videoBean)
    }

    fun getLikeVideos(success: MutableLiveData<VideoBeanResult>, failed: MutableLiveData<String>) {
        apiService.getLikeVideos()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<VideoBeanResult> {
                override fun onSubscribe(d: Disposable) {
                }

                override fun onError(e: Throwable) {
                    failed.value = e.message
                }

                override fun onComplete() {
                }

                override fun onNext(t: VideoBeanResult) {
                    success.value = t
                }
            })
    }

    fun noLikeVideo(videoId: Int, success: MutableLiveData<LikeBeanResult>, failed: MutableLiveData<String>) {
        apiService.noLikeVideo(videoId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<LikeBeanResult> {
                override fun onSubscribe(d: Disposable) {
                }

                override fun onError(e: Throwable) {
                    failed.value = e.message
                }

                override fun onComplete() {
                }

                override fun onNext(t: LikeBeanResult) {
                    success.value = t
                }
            })
    }
}