package com.huluobo.a2202kotlindouyindemo.mine

import android.content.Intent
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.blankj.utilcode.util.SPUtils
import com.flyco.tablayout.listener.CustomTabEntity
import com.flyco.tablayout.listener.OnTabSelectListener
import com.huluobo.a2202kotlindouyindemo.R
import com.huluobo.a2202kotlindouyindemo.adapter.CommonPagerAdapter
import com.huluobo.a2202kotlindouyindemo.databinding.FragmentMineBinding
import com.huluobo.a2202kotlindouyindemo.setting.SettingActivity
import com.huluobo.a2202kotlindouyindemo.utils.ImageUtils
import com.huluobo.base.bean.VideoBean
import com.huluobo.base.db.AppDBUtils
import com.huluobo.base.net.Const
import com.huluobo.base.view.BaseMVVMFragment
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers


class MineFragment : BaseMVVMFragment<FragmentMineBinding>() {
    private val tabEntity = arrayListOf<CustomTabEntity>()
    private lateinit var commonPagerAdapter: CommonPagerAdapter
    private val fragments = mutableListOf<Fragment>()
    private lateinit var viewModel: MineViewModel

    override fun getLayoutId(): Int = R.layout.fragment_mine

    override fun initData() {
        viewModel = ViewModelProvider(requireActivity())[MineViewModel::class.java]

        viewModel.getUserInfo(SPUtils.getInstance().getString(Const.PARAM_USERNAME))

        viewModel.getUserInfoSuccess.observe(this) {
            Log.i("TagA", "我的信息:$it")
            ImageUtils.loadImage(requireContext(), it.icon, dataBinding.mineUserIconIv)
            dataBinding.mineNicknameTv.text = it.nickname
        }

        viewModel.getUserInfoFailed.observe(this) {
            Log.i("TagA", "获取我的信息失败:$it")
        }
    }

    override fun initView() {
        tabEntity.clear()
        tabEntity.add(TabEntity("收藏视频", 0, 0))
        tabEntity.add(TabEntity("点赞视频", 0, 0))
        dataBinding.mineTabLayout.setTabData(tabEntity)

        fragments.clear()
        fragments.add(CollectVideoFragment())
        fragments.add(LikeVideoFragment())

        commonPagerAdapter = CommonPagerAdapter(requireActivity(), fragments)
        dataBinding.mineVp2.adapter = commonPagerAdapter

        dataBinding.mineTabLayout.setOnTabSelectListener(object : OnTabSelectListener {
            override fun onTabSelect(position: Int) {
                dataBinding.mineVp2.currentItem = position
            }

            override fun onTabReselect(position: Int) {
            }
        })

        dataBinding.mineVp2.registerOnPageChangeCallback(object : OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                dataBinding.mineTabLayout.currentTab = position
            }
        })

        dataBinding.mineSettingIv.setOnClickListener {
            val intent = Intent(requireContext(), SettingActivity::class.java)
            startActivity(intent)
        }

    }
}