package com.huluobo.a2202kotlindouyindemo.mine

import androidx.lifecycle.MutableLiveData
import com.huluobo.base.bean.LikeBeanResult
import com.huluobo.base.bean.UserBean
import com.huluobo.base.bean.VideoBean
import com.huluobo.base.bean.VideoBeanResult
import com.huluobo.base.viewModel.BaseViewModel

/**
 *  Created by LC on 2024/6/11.
 */
class MineViewModel : BaseViewModel<MineRepo>() {
    val getUserInfoSuccess = MutableLiveData<UserBean>()
    val getUserInfoFailed = MutableLiveData<String>()

    val getCollectVideosSuccess = MutableLiveData<List<VideoBean>>()
    val getCollectVideosFailed = MutableLiveData<String>()

    val getLikeVideosSuccess = MutableLiveData<VideoBeanResult>()
    val getLikeVideosFailed = MutableLiveData<String>()

    val noLikeVideoSuccess = MutableLiveData<LikeBeanResult>()
    val noLikeVideoFailed = MutableLiveData<String>()

    fun noLikeVideo(videoId: Int) {
        repo.noLikeVideo(videoId, noLikeVideoSuccess, noLikeVideoFailed)
    }

    fun getLikeVideos() {
        repo.getLikeVideos(getLikeVideosSuccess, getLikeVideosFailed)
    }

    fun deleteCollectVideo(videoBean: VideoBean) {
        repo.deleteCollectVideo(videoBean)
    }

    fun getCollectVideos() {
        repo.getCollectVideos(getCollectVideosSuccess, getCollectVideosFailed)
    }

    fun getUserInfo(username: String) {
        repo.getUserInfo(username, getUserInfoSuccess, getUserInfoFailed)
    }

    override fun createRepo(): MineRepo = MineRepo()
}