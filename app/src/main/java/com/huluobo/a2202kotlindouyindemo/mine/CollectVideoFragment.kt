package com.huluobo.a2202kotlindouyindemo.mine

import android.util.Log
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.huluobo.a2202kotlindouyindemo.R
import com.huluobo.a2202kotlindouyindemo.adapter.CollectAdapter
import com.huluobo.a2202kotlindouyindemo.databinding.FragmentCollectVideoBinding
import com.huluobo.base.bean.VideoBean
import com.huluobo.base.view.BaseMVVMFragment

class CollectVideoFragment : BaseMVVMFragment<FragmentCollectVideoBinding>() {
    private lateinit var viewModel: MineViewModel
    private lateinit var collectAdapter: CollectAdapter
    private val collectList = mutableListOf<VideoBean>()
    override fun getLayoutId(): Int = R.layout.fragment_collect_video

    override fun initData() {
        viewModel = ViewModelProvider(requireActivity())[MineViewModel::class.java]

        viewModel.getCollectVideos()

        viewModel.getCollectVideosSuccess.observe(this) {
            collectList.clear()
            collectList.addAll(it)
            collectAdapter.notifyDataSetChanged()
        }

        viewModel.getCollectVideosFailed.observe(this) {
            Log.i("TagA", "获取收藏视频列表失败:$it")
        }
    }

    override fun initView() {
        dataBinding.collectRv.layoutManager = GridLayoutManager(requireContext(), 2)
        collectAdapter = CollectAdapter(collectList)
        dataBinding.collectRv.adapter = collectAdapter

        collectAdapter.addChildClickViewIds(R.id.item_collect_delete_btn)

        collectAdapter.setOnItemChildClickListener { adapter, view, position ->
            when (view.id) {
                R.id.item_collect_delete_btn -> {
                    viewModel.deleteCollectVideo(collectList[position])
                }
            }
        }
    }
}