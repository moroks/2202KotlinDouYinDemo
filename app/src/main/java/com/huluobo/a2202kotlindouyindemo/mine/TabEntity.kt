package com.huluobo.a2202kotlindouyindemo.mine

import com.flyco.tablayout.listener.CustomTabEntity

/**
 *  Created by LC on 2024/6/8.
 */
data class TabEntity(
    val title: String, val selectIcon: Int, val unSelectIcon: Int
) : CustomTabEntity {
    override fun getTabTitle(): String = title

    override fun getTabSelectedIcon(): Int = selectIcon

    override fun getTabUnselectedIcon(): Int = unSelectIcon
}
