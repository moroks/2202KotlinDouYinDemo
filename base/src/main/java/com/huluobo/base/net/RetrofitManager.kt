package com.huluobo.base.net

import com.blankj.utilcode.util.SPUtils
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 *  Created by LC on 2024/5/23.
 */
class RetrofitManager {
    companion object {
        fun getRetrofit(): Retrofit {
            //Okhttp
            val okHttpBuilder = OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(Interceptor {
                    val token = SPUtils.getInstance().getString(Const.PARAM_TOKEN)
                    val newBuilder = it.request().newBuilder()
                    newBuilder.addHeader(Const.PARAM_TOKEN, token)
                    it.proceed(newBuilder.build())
                })
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))

            //Retrofit
            val retrofit = Retrofit.Builder()
                .baseUrl(Const.baseURL)
                .client(okHttpBuilder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())

            return retrofit.build()
        }
    }
}