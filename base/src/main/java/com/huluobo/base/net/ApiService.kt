package com.huluobo.base.net

import com.huluobo.base.bean.AddToCartBeanResult
import com.huluobo.base.bean.CartBeanResult
import com.huluobo.base.bean.CommentBean
import com.huluobo.base.bean.CommentBeanResult
import com.huluobo.base.bean.FollowBeanResult
import com.huluobo.base.bean.GiftBeanResult
import com.huluobo.base.bean.GoodsBeanResult
import com.huluobo.base.bean.LikeBeanResult
import com.huluobo.base.bean.UserBeanResult
import com.huluobo.base.bean.VideoBeanResult
import io.reactivex.Observable
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

/**
 *  Created by LC on 2024/5/23.
 */
interface ApiService {
    @POST(Const.URL_LOGIN)
    fun login(@Body body: RequestBody): Observable<UserBeanResult>

    @GET(Const.URL_VIDEOS)
    fun getVideos(
        @Query(Const.PARAM_CURRENT_PAGE) currentPage: Int,
        @Query(Const.PARAM_PAGE_SIZE) pageSize: Int
    ): Observable<VideoBeanResult>

    @POST(Const.URL_LIKE_VIDEO)
    fun likeVideo(@Query(Const.PARAM_VIDEO_ID) videoId: Int): Observable<LikeBeanResult>

    @POST(Const.URL_NO_LIKE_VIDEO)
    fun noLikeVideo(@Query(Const.PARAM_VIDEO_ID) videoId: Int): Observable<LikeBeanResult>

    @POST(Const.URL_FOLLOW_AUTH)
    fun followAuth(@Body body: RequestBody): Observable<FollowBeanResult>

    @GET(Const.URL_COMMENT)
    fun getComment(@Query(Const.PARAM_VIDEO_ID) videoId: Int): Observable<CommentBeanResult>

    @POST(Const.URL_SEND_COMMENT)
    fun sendComment(@Body body: RequestBody): Observable<CommentBeanResult>

    @POST(Const.URL_NO_FOLLOW_AUTH)
    fun noFollowAuth(@Body body: RequestBody): Observable<FollowBeanResult>

    @GET(Const.URL_GET_GIFT)
    fun getGiftList(): Observable<GiftBeanResult>

    @GET(Const.URL_GET_GOODS_LIST)
    fun getGoodsList(
        @Query(Const.PARAM_CATEGORY_ID) categoryId: Int,
        @Query(Const.PARAM_CURRENT_PAGE) currentPage: Int,
        @Query(Const.PARAM_PAGE_SIZE) pageSize: Int
    ): Observable<GoodsBeanResult>

    @GET(Const.URL_GET_CART_LIST)
    fun getCartList(): Observable<CartBeanResult>

    @POST(Const.URL_ADD_TO_CART)
    fun addToCart(@Body body: RequestBody): Observable<AddToCartBeanResult>

    @GET(Const.URL_FIND_VIDEOS)
    fun findVideos(@Query(Const.PARAM_KEYWORD) keywords: String): Observable<VideoBeanResult>

    @POST(Const.URL_GET_LIKE_VIDEOS)
    fun getLikeVideos(): Observable<VideoBeanResult>
}