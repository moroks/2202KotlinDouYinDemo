package com.huluobo.base.net

/**
 *  Created by LC on 2024/5/23.
 */
class Const {
    companion object {
        const val baseURL = "http://10.161.9.80:7012"

        const val PARAM_TOKEN = "token"

        const val PARAM_USERNAME = "username"

        const val PARAM_PASSWORD = "password"

        const val URL_LOGIN = "/user/loginjson"

        const val MEDIA_TYPE = "application/json;charset=utf-8"

        const val PARAM_CURRENT_PAGE = "currentPage"

        const val PARAM_PAGE_SIZE = "pageSize"

        const val URL_VIDEOS = "/video/findVideos"

        const val URL_LIKE_VIDEO = "/fav/love"

        const val URL_NO_LIKE_VIDEO = "/fav/nolove"

        const val PARAM_VIDEO_ID = "videoId"

        const val URL_FOLLOW_AUTH = "/guanzhu/guanzhu"

        const val PARAM_AUTHNAME = "authname"

        const val URL_COMMENT = "/comment/getCommentByVideoId"

        const val URL_SEND_COMMENT = "/comment/comment"

        const val PARAM_MSG = "msg"

        const val URL_NO_FOLLOW_AUTH = "/guanzhu/noguanzhu"

        const val URL_GET_GIFT = "/gift/json"

        const val CHAT_ROOM_ID = "249651126140934"

        const val PARAM_CATEGORY_ID = "category_id"

        const val URL_GET_GOODS_LIST = "/goods/info"

        const val URL_GET_CART_LIST = "/goods/selectCar"

        const val PARAM_GOODS_ID = "goods_id"

        const val PARAM_COUNT = "count"

        const val URL_ADD_TO_CART = "/goods/addCar"

        const val PARAM_KEYWORD = "keywords"

        const val URL_FIND_VIDEOS = "/video/findVideoByName"

        const val URL_GET_LIKE_VIDEOS = "/fav/loveVideos"
    }
}