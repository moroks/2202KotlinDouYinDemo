package com.huluobo.base.model

import com.blankj.utilcode.util.ApiUtils.Api
import com.huluobo.base.db.AppDBUtils
import com.huluobo.base.db.AppDatabase
import com.huluobo.base.net.ApiService
import com.huluobo.base.net.RetrofitManager

/**;
 *  Created by LC on 2024/5/23.
 *  val apiService: ApiService = RetrofitManager.getRetrofit().create(ApiService::class.java)
 */
abstract class BaseRepo(
    val apiService: ApiService = RetrofitManager.getRetrofit().create(ApiService::class.java),
    val db: AppDatabase = AppDBUtils.getDb()
) {
}