package com.huluobo.base.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.huluobo.base.bean.VideoBean
import io.reactivex.Observable

/**
 *  Created by LC on 2024/5/29.
 */
@Dao
interface VideoDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertVideo(videoBean: VideoBean)

    @Delete
    fun deleteVideo(videoBean: VideoBean)

    @Update
    fun updateVideo(videoBean: VideoBean)

    @Query("select * from video_info")
    fun queryVideos(): Observable<List<VideoBean>>
}