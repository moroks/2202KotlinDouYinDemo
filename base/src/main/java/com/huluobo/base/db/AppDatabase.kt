package com.huluobo.base.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.huluobo.base.bean.Money
import com.huluobo.base.bean.SearchBeanResult
import com.huluobo.base.bean.UserBean
import com.huluobo.base.bean.VideoBean

/**
 *  Created by LC on 2024/5/27.
 */
@Database(
    entities = [UserBean::class, VideoBean::class, Money::class, SearchBeanResult::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getUserInfoDao(): UserDao

    abstract fun getVideoDao(): VideoDao

    abstract fun getMoneyDao(): MoneyDao

    abstract fun getSearchDao(): SearchDao
}