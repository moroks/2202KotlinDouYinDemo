package com.huluobo.base.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.huluobo.base.bean.UserBean
import io.reactivex.Observable

/**
 *  Created by LC on 2024/5/27.
 */
@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUserInfo(userBean: UserBean)

    @Delete
    fun deleteUserInfo(userBean: UserBean)

    @Update
    fun updateUserInfo(userBean: UserBean)

    @Query("select * from user_info")
    fun queryAllUserInfo(): List<UserBean>

    @Query("select * from user_info where username=:username")
    fun queryUserInfoByName(username: String): Observable<UserBean>
}