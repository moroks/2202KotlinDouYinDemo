package com.huluobo.base.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.huluobo.base.bean.SearchBeanResult
import io.reactivex.Observable

/**
 *  Created by LC on 2024/6/7.
 */
@Dao
interface SearchDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertKeywords(searchBeanResult: SearchBeanResult)

    @Delete
    fun deleteKeywords(searchBeanResult: SearchBeanResult)

    @Update
    fun updateKeywords(searchBeanResult: SearchBeanResult)

    @Query("select * from search_info")
    fun queryKeywords(): Observable<List<SearchBeanResult>>
}