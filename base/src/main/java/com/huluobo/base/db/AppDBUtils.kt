package com.huluobo.base.db

import android.content.Context
import androidx.room.Room

/**
 *  Created by LC on 2024/5/27.
 */
object AppDBUtils {
    private lateinit var database: AppDatabase

    fun init(context: Context) {
        database = Room.databaseBuilder(context, AppDatabase::class.java, "douyin.db")
            .allowMainThreadQueries()
            .build()
    }

    fun getDb(): AppDatabase = database
}