package com.huluobo.base.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.huluobo.base.bean.Money
import io.reactivex.Observable

/**
 *  Created by LC on 2024/6/3.
 */
@Dao
interface MoneyDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUserMoney(money: Money)

    @Delete
    fun deleteUserMoney(money: Money)

    @Update
    fun updateUserMoney(money: Money)

    @Query("select * from user_money where user_name=:username")
    fun queryUserMoneyByName(username: String): Observable<Money>
}