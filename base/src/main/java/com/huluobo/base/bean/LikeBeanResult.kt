package com.huluobo.base.bean

/**
 *  Created by LC on 2024/5/27.
 */
data class LikeBeanResult(
    val code: Int,
    val `data`: String,
    val message: String
)