package com.huluobo.base.bean;

/**
 * Created by LC on 2024/6/7.
 */
public class SearchBeanResultJava {
    private int id;
    private String keyword;

    public SearchBeanResultJava(String keyword) {
        this.keyword = keyword;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}
