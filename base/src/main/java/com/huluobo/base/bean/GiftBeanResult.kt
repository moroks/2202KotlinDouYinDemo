package com.huluobo.base.bean

/**
 *  Created by LC on 2024/6/3.
 */
data class GiftBeanResult(
    val code: Int,
    val `data`: List<GiftBean>,
    val message: String
)

data class GiftBean(
    val giftname: String,
    val giftpath: String,
    val id: Int,
    val price: Int
)