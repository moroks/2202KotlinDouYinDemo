package com.huluobo.base.bean

/**
 *  Created by LC on 2024/6/5.
 */
data class AddToCartBeanResult(
    val code: Int,
    val `data`: Any,
    val message: String
)