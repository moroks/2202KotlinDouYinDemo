package com.huluobo.base.bean

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 *  Created by LC on 2024/5/23.
 */
data class UserBeanResult(
    val code: Int,
    val `data`: UserBean,
    val message: String
)

@Entity(tableName = "user_info")
data class UserBean(
    val address: String?,
    val admin: Boolean,
    val birth: String?,
    val coinCount: Int,
    val email: String?,
    val icon: String,
    @PrimaryKey
    val id: Int,
    val nickname: String?,
    val password: String,
    val publicName: String?,
    val sex: String?,
    val token: String,
    val type: Int,
    val username: String
)