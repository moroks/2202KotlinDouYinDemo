package com.huluobo.base.bean

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.chad.library.adapter.base.entity.MultiItemEntity
import java.io.Serializable

/**
 *  Created by LC on 2024/5/24.
 */
data class VideoBeanResult(
    val code: Int,
    val `data`: List<VideoBean>,
    val message: String
)

@Entity(tableName = "video_info")
data class VideoBean(
    val address: String?,
    val authname: String,
    val caption: String,
    var dianzan: Int,
    var guanzhu: Int,
    val headpath: String,
    @PrimaryKey
    val id: Int,
    val like_count: Int,
    val publishtime: String,
    val type: Int,
    val videomainimg: String,
    val videopath: String,
    val view_count: Int
) : MultiItemEntity,Serializable {
    override val itemType: Int
        get() = type
}