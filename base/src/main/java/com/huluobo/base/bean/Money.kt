package com.huluobo.base.bean

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 *  Created by LC on 2024/6/3.
 */
@Entity(tableName = "user_money")
data class Money(
    @PrimaryKey(autoGenerate = true)
    val id: Long,
    @ColumnInfo(name = "user_name")
    val username: String,
    @ColumnInfo(name = "user_money")
    var money: Float
)