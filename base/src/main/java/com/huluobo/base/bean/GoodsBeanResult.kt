package com.huluobo.base.bean

/**
 *  Created by LC on 2024/6/5.
 */
data class GoodsBeanResult(
    val code: Int,
    val `data`: List<GoodsBean>,
    val message: String
)

data class GoodsBean(
    val bannerList: List<String>,
    val category_id: Int,
    val goods_attribute: String,
    val goods_banner: String,
    val goods_code: String,
    val goods_default_icon: String,
    val goods_default_price: Int,
    val goods_desc: String,
    val goods_detail_one: String,
    val goods_detail_two: String,
    val goods_sales_count: Int,
    val goods_stock_count: Int,
    val id: Int
)