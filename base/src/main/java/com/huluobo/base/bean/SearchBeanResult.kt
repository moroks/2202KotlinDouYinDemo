package com.huluobo.base.bean

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 *  Created by LC on 2024/6/7.
 */
@Entity(tableName = "search_info")
data class SearchBeanResult(
    val keyword: String,
    @PrimaryKey(autoGenerate = true)
    val id: Int
)