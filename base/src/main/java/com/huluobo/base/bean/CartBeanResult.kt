package com.huluobo.base.bean

/**
 *  Created by LC on 2024/6/5.
 */
data class CartBeanResult(
    val code: Int,
    val `data`: List<CartBean>,
    val message: String
)

data class CartBean(
    val count: Int,
    val goods_default_icon: String,
    val goods_default_price: Int,
    val goods_desc: String,
    val goods_id: Int,
    val id: Int,
    val order_id: Int,
    val user_id: Int
)