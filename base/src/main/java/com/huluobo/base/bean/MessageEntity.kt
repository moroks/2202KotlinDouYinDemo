package com.huluobo.base.bean

/**
 *  Created by LC on 2024/6/4.
 */
data class MessageEntity(
    val username: String,
    val content: String
)