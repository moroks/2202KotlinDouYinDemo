package com.huluobo.base.bean

/**
 *  Created by LC on 2024/5/28.
 */
data class CommentBeanResult(
    val code: Int,
    val `data`: List<CommentBean>,
    val message: String
)

data class CommentBean(
    val childcount: Int,
    val createtime: String,
    val icon: String,
    val id: Int,
    val msg: String,
    val parentid: Int,
    val rootid: Int,
    val tousername: String,
    val type: Int,
    val username: String
)