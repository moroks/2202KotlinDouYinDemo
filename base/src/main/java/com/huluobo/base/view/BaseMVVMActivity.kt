package com.huluobo.base.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.huluobo.base.model.BaseRepo
import com.huluobo.base.viewModel.BaseViewModel

/**
 *  Created by LC on 2024/5/23.
 *  lateinit 延迟初始化 被该关键字标记的对象,可以在创建类的时候不给初始化的值,可以后面实现的时候再赋值
 *  延迟初始化的意义在于,声明时可以不赋空值,在之后运行的过程中,可以进行赋值
 */
abstract class BaseMVVMActivity<VDB : ViewDataBinding> : AppCompatActivity() {
    protected lateinit var dataBinding: VDB

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding = DataBindingUtil.setContentView(this, getLayoutId())
        initView()
        initData()
    }

    abstract fun initData()

    abstract fun initView()

    abstract fun getLayoutId(): Int
}