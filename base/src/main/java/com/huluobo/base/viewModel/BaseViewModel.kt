package com.huluobo.base.viewModel

import androidx.lifecycle.ViewModel
import com.huluobo.base.model.BaseRepo

/**
 *  Created by LC on 2024/5/23.
 */
abstract class BaseViewModel<Repo : BaseRepo> : ViewModel() {
    val repo: Repo = this.createRepo()

    abstract fun createRepo(): Repo
}